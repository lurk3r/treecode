#include <iostream>
#include "treecode.h"
#include <chrono>
#include "omp.h"

using namespace std;

int main() {

    // using int32_t as ordering, maximum (2^7)^4 elements in matrix.
    // otherwise causes overflow.
    auto qt = new treecode(0, 0, 1., 6);
    for (auto it : qt->root->points) {it->attribute = 5.1;}
    qt->root->populate();

    scalar_t theta = 0.7;

    level_t n = qt->size * qt->size;
//    auto matrix = new scalar_t[n * n];

    auto i = 0;
    auto t0 = chrono::system_clock::now();

    omp_set_num_threads(4);
    /*
     * spawning is slow. more threads can make it faster
     * about 25% faster.
     */
//#pragma omp parallel for private(i) shared(qt, theta, n, matrix)
//    for (i = 0; i < n; i++) {
//        traversal(qt, theta, qt->root->points[i], qt->root, n, matrix);
//    }
    auto t1 = chrono::system_clock::now();
//
//    std::cout << "matrix setting uses " <<
//            chrono::duration_cast<chrono::milliseconds>(t1 - t0).count() << " milliseconds"
//    << std::endl;



    auto lhs = new scalar_t[n];
    auto rhs = new scalar_t[n];

    t0 = chrono::system_clock::now();
    traversal_down(qt->root, rhs);
    traversal_up(qt->root);

    t1 = chrono::system_clock::now();

    std::cout << "pre-processing uses " <<
    chrono::duration_cast<chrono::milliseconds>(t1 - t0).count() << " milliseconds"
    << std::endl;


    t0 = chrono::system_clock::now();
#pragma omp parallel for private(i) shared(qt, theta, n, lhs, rhs)
    for (i = 0; i < n; i++) {
        traversal(qt, theta, qt->root->points[i], qt->root, n, lhs, rhs);
    }
    t1 = chrono::system_clock::now();

    std::cout << "mulpilcation  uses " <<
    chrono::duration_cast<chrono::milliseconds>(t1 - t0).count() << " milliseconds"
    << std::endl;


    t0 = chrono::system_clock::now();
#pragma omp parallel for private(i) shared(qt, theta, n, lhs, rhs)
    for (i = 0; i < n; i++) {
        fast_traversal(qt, theta, qt->root->points[i], qt->root, n, lhs, rhs);
    }
    t1 = chrono::system_clock::now();

    std::cout << "mulpilcation  uses " <<
    chrono::duration_cast<chrono::milliseconds>(t1 - t0).count() << " milliseconds"
    << std::endl;


    auto sum = 0;
    for (auto i = 0; i < qt->interactions.size(); i++) {
        sum += qt->interactions[i].size();
    }

    std::cout << "total interactions " << sum/qt->interactions.size()<< std::endl;
//    delete[] matrix;
    delete[] lhs;
    delete[] rhs;
    delete qt;
    return 0;

}